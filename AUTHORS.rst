=======
Credits
=======

Maintainer
----------

* Yan Xie <xieyan@msu.edu>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst
