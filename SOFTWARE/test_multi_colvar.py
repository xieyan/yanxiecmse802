import pandas as pd
import pytest
from multi_colvar import multi_colvar

def test_good_multi_colvar():
    #test a good set of colvar files
    import os
    print(os.getcwd())
    assert len(multi_colvar(3))==3
     
def test_bad_multi_colvar():
    # test a bad load example of multiple colvars
    assert len(multi_colvar(5))!=4 