import pandas as pd
import pytest
from multi_colvar import multi_colvar
from distance_plot import distance_plot

def test_distance_plot():
    dc=multi_colvar(n=4)
    assert distance_plot(dc)

