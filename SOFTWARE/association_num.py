import numpy as np
def association_num(dc):
    '''
    This function is to  find out the interacting residues by defining the distance greater than 0.5 nm to np.nan and less than 0.5 nm to 1, meaning association.
    
    Parameters:
    
    dc: pandas dataframe. The first column of dc is time, and the following columns are all distances with each residue. 
    
    Returns:
    
    dc: pandas dataframe. The first column of dc is time, and the following columns contain either 1 or np.nan. When it's 1, g6p is interacting with the corresponding residue. np.nan means no interaction. The last column num is the number of association at certain time.
    
    '''
    for m in range(0,len(dc)):
        for j in range(1,9):
            for i in range(0,len(dc[m])):
                if dc[m][dc[m].columns[j]][i]>0.5:
                    dc[m][dc[m].columns[j]][i]=np.nan
                else:
                    dc[m][dc[m].columns[j]][i]=1
        num=dc[m][dc[m].columns[1:9]].count(axis='columns')
        dc[m]['num']=num
    return dc