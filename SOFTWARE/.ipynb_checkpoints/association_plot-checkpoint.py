import matplotlib.pyplot as plt

def association_plot(dc,dxvg):
    '''
    This function is used to plot the distance changes, association numbers and the interaction energy between single residue and intermediate, and the interaction energy with the peptide all as a function of simulation time.
   
    Parameters:
    
    dc: pandas dataframe. The first column of dc is time, and the following columns are all distances with each residue. 
    
    dxvg: pandas dataframe. The first column of dxvg is time, and the following columns are the interaction energy between each residue and g6p.
   
    Returns:
    
    This function is to make plots and it doesn't return anything.
    
    '''
    if len(dc)!=len(dxvg):
        raise ValueError("Length of inputs does't match.")
    else:
        for j in range(0,len(dc)):
            fig,axs=plt.subplots(4,1,figsize=(8,1*(len(dc)+len(dxvg))))
            for i in range(1,9):
                axs[0].scatter(dc[j]['time'],dc[j][dc[j].columns[i]]*i)
                axs[0].set_yticks([1,2,3,4,5,6,7,8])
                axs[0].set_title(str(j+1)+': Distance: association residue indices ',color='k',fontsize=15)
                axs[0].set_ylabel('Res index', fontsize=12)
                axs[0].set_xticks(range(0,10),minor=True)
                axs[0].yaxis.set_tick_params(labelsize=10)
                axs[0].get_yticklabels()[1].set_color('darkorange') 
                axs[0].get_yticklabels()[3].set_color('red') 
                axs[0].get_yticklabels()[5].set_color('maroon') 

                axs[1].plot(dc[j]['time'],dc[j]['num'],'red',alpha=0.1)
                axs[1].set_yticks([1,2,3])
                axs[1].set_ylim([-0.2,4])
                axs[1].tick_params(axis='y', which='both', colors='r',direction='in', pad=-15)
                axs[1].set_ylabel('Association', fontsize=12)
                axs[1].set_xticks(range(0,10),minor=True)
                axs[1].plot([0,10],[3,3],linewidth=0.1,color='grey')
                axs[1].plot([0,10],[2,2],linewidth=0.1,color='grey')
                axs[1].plot([0,10],[1,1],linewidth=0.1,color='grey')
                axs[1].text(0.025, 0.35, '1',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[1].text(0.025, 0.7, '2',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[1].text(0.025, 0.9, '3',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[1].set_title('Distance: association number ',color='k',fontsize=15)
                axs[1].set_xlabel('Time / ns')

                axs[2].plot(dxvg[j]['time'],dxvg[j]['ie_'+str(i+2)])
                axs[2].set_ylabel('IE / kJ mol$^{-1}$', fontsize=10)
                axs[2].set_xticks(range(0,10),minor=True)
                axs[2].yaxis.get_ticklocs(minor=True)   
                axs[2].minorticks_on()
                axs[2].set_title('Interaction energy with each residue ',color='k',fontsize=15)

                axs[3].plot(dxvg[j]['time'],dxvg[j]['ie_tot'],c='b')
                axs[3].set_ylabel('Total IE / kJ mol$^{-1}$', fontsize=10)
                axs[3].set_yticks([-400,-350,-300,-250,-200,-150,-100,-50,0])
                axs[3].yaxis.set_tick_params(labelsize=7)
                axs[3].plot([0,10],[-160,-160],linewidth=0.1,color='grey')
                axs[3].plot([0,10],[-290,-290],linewidth=0.1,color='grey')
                axs[3].plot([0,10],[-410,-410],linewidth=0.1,color='grey')
                axs[3].set_xlabel('Time / ns')
                axs[3].text(0.025, 0.08, '3',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[3].text(0.025, 0.33, '2',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[3].text(0.025, 0.6, '1',color='r', transform=plt.gca().transAxes, fontsize=15)
                axs[3].set_title('Interaction energy with the peptide ',color='k',fontsize=15)
            fig.subplots_adjust(hspace=0.7)
    return True
