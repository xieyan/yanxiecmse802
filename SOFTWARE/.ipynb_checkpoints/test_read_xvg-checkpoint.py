import pandas as pd
import pytest
from read_xvg import read_xvg


def test_read_good_xvg():
    #test a good xvg file
    import os
    print(os.getcwd())
    assert type(read_xvg('Data/good_xvg.data',verbose=True)) == pd.DataFrame   

def test_read_missing_colvar():
    # test a file with header
    with pytest.raises(FileNotFoundError) as excinfo:
        read_xvg("Data/missing.data")
    assert "No such file or directory" in str(excinfo.value)