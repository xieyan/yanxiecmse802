import pandas as pd
import pytest
from read_colvar import read_colvar


def test_read_good_colvar():
    #test a good colvar file
    import os
    print(os.getcwd())
    assert type(read_colvar('Data/good_colvar.data',verbose=True)) == pd.DataFrame   
def test_read_bad_colvar():
    # test a file with header
    with pytest.raises(AssertionError) as excinfo:
        read_colvar("Data/bad_colvar.data")
    assert 'Missing or incorrect header' == str(excinfo.value)
    
def test_read_missing_colvar():
    # test a nonexsit file
    with pytest.raises(FileNotFoundError) as excinfo:
        read_colvar("Data/missing.data")
    assert "No such file or directory" in str(excinfo.value)