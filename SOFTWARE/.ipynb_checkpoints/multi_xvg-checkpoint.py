from read_xvg import read_xvg

def multi_xvg(n=1):
    '''
    This function is to read multiple xvg files and put them into one dataframe.
    
    Parameters:
    
    n: number of colvar files you want to load. This is dependent on how many data files you have. n cannot beyond the total number of files
    
    Returns:
    
    dxvg: pandas dataframe. Every page is from an independent simulation. The first column of dc is time, and the following columns are all interaction energy with each residue. 
    
    '''
    
    dxvg=[]
    for i in range(1,n+1):
        dd=read_xvg('Data/interaction_energy_'+str(i)+'.xvg',verbose=True)
        dxvg.append(dd)
    return dxvg