import matplotlib.pyplot as plt
def distance_plot(dc):
    '''
    Plot the distance changes over time.
    
    Parameters:
    
    dc: pandas dataframe. The first column of dc is time, and the following columns are all distances with each residue. 
    
    Returns:
    
    This is a plot funtion, no returns.
    '''
    fig, axs = plt.subplots(len(dc),1,figsize=(8, 4*len(dc)) , squeeze=True)
    for j in range(0,len(dc)):
        for i in range(1,9):
            axs[j].plot(dc[j].time,dc[j][dc[j].columns[i]],label='d'+str(i))
            axs[j].legend(loc='upper left',fontsize='xx-small')
            axs[j].set_title(str(j+1),color='red')
            axs[j].set_ylabel('Distance /nm')
            axs[j].set_xticks(range(0,10),minor=True)
            axs[j].yaxis.get_ticklocs(minor=True)   
            axs[j].minorticks_on()
            axs[j].plot([-0.5,10.5],[0.55,0.55],'k')
            axs[j].set_xlim(-1,10.5)
            axs[j].set_xlabel('Simulation time /ns')
            fig.subplots_adjust(hspace=0.7)
    plt.show()           
    return True 