import pandas 
import gromacs as gmx
XVG= gmx.fileformats.xvg.XVG

def read_xvg(fname,verbose=False):
    '''
    Load xvg file.
    
    Parameters:
    
    fname: Str, the name of colvar file. 
    
    verbose: False or True. False hides the loaded file's name, and True shows the name.
    
    Returns:
    
    dxvg: return a dataframe that contains the simulation time and distances
    '''
    if verbose: print(fname)
        
    dd=XVG(filename=fname)
    time=dd.array[0]/1000
    ie_3=dd.array[1]+dd.array[2]
    ie_4=dd.array[3]+dd.array[4]
    ie_5=dd.array[5]+dd.array[6]
    ie_6=dd.array[7]+dd.array[8]
    ie_7=dd.array[9]+dd.array[10]
    ie_8=dd.array[11]+dd.array[12]
    ie_9=dd.array[13]+dd.array[14]
    ie_10=dd.array[15]+dd.array[16]
    ie_tot=dd.array[1]+dd.array[2]+dd.array[3]+dd.array[4]\
    +dd.array[5]+dd.array[6]+dd.array[7]+dd.array[8]+dd.array[9]+dd.array[10]\
    +dd.array[11]+dd.array[12]+dd.array[13]+dd.array[14]+dd.array[15]+dd.array[16]
    dxvg=pandas.DataFrame({'time':time,'ie_3':ie_3,'ie_4':ie_4,'ie_5':ie_5,'ie_6':ie_6,'ie_7':ie_7,\
                                       'ie_8':ie_8,'ie_9':ie_9,'ie_10':ie_10,'ie_tot':ie_tot})
    return dxvg