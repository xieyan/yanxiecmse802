==============
YanXieCMSE802 
==============

.. image:: https://img.shields.io/travis/xieyan/yanxiecmse802-.svg
        :target: https://travis-ci.org/xieyan/yanxiecmse802-

.. image:: https://img.shields.io/pypi/v/yanxiecmse802-.svg
        :target: https://pypi.python.org/pypi/yanxiecmse802-


Repository for or Yr Yan Xie's CMSE802 files.

* Free software: 3-clause BSD license
* Documentation: (COMING SOON!) https://xieyan.github.io/yanxiecmse802-.

Features
--------

* TODO
