.. CMSE802YANXie documentation master file, created by
   sphinx-quickstart on Sat Oct 26 20:24:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CMSE802YANXie's documentation!
=========================================

This is my auto documentation!!!

.. toctree::
   :maxdepth: 2
   :caption: Contents:
  
   tutorial.rst
   project.rst
 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
*  :ref:`search`
